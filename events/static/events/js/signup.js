$('#signup').addClass('active')

function submitForm(){
  $('#page-loader').show()  
  $('#error_div').addClass('hide')
  $('#spinner').removeClass('hide')  
  $('#submit_btn').prop('disabled', true)  

  var selectedEventOptions = $('#categorySelect').find(":selected")
  var selectedEvents = []
  for(var i = 1; i < selectedEventOptions.length; i++){
  	selectedEvents.push(parseInt(selectedEventOptions[i].value))
  }
  data = {
    csrfmiddlewaretoken:CSRF_TOKEN,
    Name:$('#full_name').val(),
	  EmailId:$('#email_id').val(),
    MobileNumber:$('#mobile').val(),
    password1:$('#password').val(),
    password2:$('#confirm_password').val(),
    selectedevent:selectedEvents,
    Notifications:$('#notificationChk').is(':checked')
  }
  var validationMsg = validateForm(data)
  
  if(validationMsg != ""){
    trackGAEvent('SIGNUP', 'VALIDATION', 'VALIDATION_MSG',validationMsg)
    $('#error_div').first().removeClass('hide')
    $('#error_div').first().text(validationMsg)
    $('#page-loader').hide()
    $('#spinner').addClass('hide')
    $('#submit_btn').prop('disabled', false)
    return
  }

  $.ajax({
            type: 'POST',
            url: '',
            data: data,
            dataType: 'json',
            success: function(data) {
                if(data.isValid){
                  trackGAEvent('SIGNUP', 'SUCCESS', 'USER_SIGNED_UP',data.msg)
                  Materialize.toast(data.msg, 12000)
                  window.location.href = '/'
                }
                else{
                  trackGAEvent('SIGNUP', 'VALIDATION', 'VALIDATION_MSG',data.msg)
                  $('#page-loader').hide()
                  $('#error_div').first().removeClass('hide')
                  $('#error_div').first().text(data.msg)
                  $('#spinner').addClass('hide')
                  $('#submit_btn').prop('disabled', false)
                }
            },
            error:function(data, error){
              trackGAEvent('SIGNUP', 'ERROR', 'API_ERROR', error)
              $('#page-loader').hide()
              $('#error_div').first().removeClass('hide')
              $('#spinner').addClass('hide')
              $('#submit_btn').prop('disabled', false)
              $('#error_div').first().text("Something went wrong. Please try again!")
              console.log(error)
            }        
        });
}

function validateForm(data){
	var msg = ""
	var nameRegex = new RegExp(/^[a-zA-Z\s]*$/)
	var emailRegex = new RegExp(/^([A-Z|a-z|0-9](\.|_){0,1})*[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])*((\.){0,1}[A-Z|a-z|0-9]){1}\.[a-z]{2,}$/)
	var mobileRegex = new RegExp(/^[7-9][\d]{9}$/)
	var pwdRegex = new RegExp(/^[a-zA-Z0-9@]*$/)
	
  if(!data.Name){
		msg = "Please enter your name"
	}
	else if(!nameRegex.test(data.Name)){
		msg = "Only alphabets allowed in name. Check again?"
	}
	else if(!data.EmailId){
		msg = "Please enter your email id"
	}
	else if(!emailRegex.test(data.EmailId)){
		msg = "Oh! That doesn't look a valid email id"
	}
	else if(!data.MobileNumber){
		msg = "Please enter your mobile number"
	}
	else if(!mobileRegex.test(data.MobileNumber)){
		msg = "Oh! That doesn't look a valid mobile number"
	}
	else if(!data.password1){
		msg = "Please enter your password"
	}
	else if(data.password1.length < 5){
		msg = 'Password should be atleast 5 characters long'
	}
	else if(!pwdRegex.test(data.password1)){
		msg = 'Password can only contain alphabets, numbers and @ symbol'
	}
	else if(data.password1 != data.password2){
		msg = "Password and confirmed password do not match"
	}
	else if(data.selectedevent.length < 2){
		msg = "Please select atleast 2 preferred events"
	}

	return msg
}

function startLoader(){
  debugger;
  $('#page-loader').show()  
}

$(document).ready(function() {
	$('select').material_select();
});