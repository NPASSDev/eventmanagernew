//adding active class to navbar tab
$('#login').addClass('active')

function submitForm(){
  $('#page-loader').show()
  $('#error_div').addClass('hide')  
  var emailId = $('#email_id').val()
  var pwd = $('#password').val()
  data = {
    csrfmiddlewaretoken:CSRF_TOKEN,
    EmailId:emailId,
    Password:pwd
  }
  $.ajax({
            type: 'POST',
            url: '',
            data: data,
            dataType: 'json',
            success: function(data) {
                if(data.isValid){
                  trackGAEvent('LOGIN', 'SUCCESS', 'USER_LOGIN',data.msg)
                  Materialize.toast(data.msg, 12000)
                  window.location.href = '/'
                }
                else{
                  trackGAEvent('LOGIN', 'VALIDATION', 'VALIDATION_MSG',data.msg)
                  $('#page-loader').hide()
                  $('#error_div').first().removeClass('hide')
                  $('#error_div').first().text(data.msg)
                }
            },
            error:function(data, error){
              trackGAEvent('LOGIN', 'ERROR', 'API_ERROR',error)
              $('#page-loader').hide()
              $('#error_div').first().removeClass('hide')
              $('#error_div').first().text("Something went wrong. Please try again!")
              console.log(error)
            }        
        });
}

function startLoader(){
  debugger;
  $('#page-loader').show()  
}