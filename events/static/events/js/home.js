//adding active class to navbar tab
$('#home').addClass('active')
$('#flyout-menu-trigger').removeClass('hide')

function handleSMSAdClick(){
	trackGAEvent('HOME', 'CLICK', 'SMS91_AD','')
}

function handleContactNowClick(){
	trackGAEvent('HOME', 'CLICK', 'CONTACT_NOW','')
}

function handleAreaModalOpen(){
	trackGAEvent('HOME', 'CLICK', 'AREA_CHANGE_MODAL','')
}

function handleCategoryChange(catId){
	trackGAEvent('HOME', 'CLICK', 'CATEGORY_FILTERED',catId)
	window['allEvents'].forEach(function(event){
		if(event.fields.EventCategoryId == catId)
			$('#e'+event.pk).first().toggle();
	})
}

function handleCityChange(){
	var area = $('#areaSelect').val()
	trackGAEvent('HOME', 'CLICK', 'CITY_CHANGE',area)
	if(cityName != area){
	//	document.cookie = AREA_COOKIE + "=" + area + ";"
		$('#page-loader').show()  
		window.location.href='/'+area;
	}
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function handleDetailModal(eventName, eventDesc, contactNo, emailId){
	trackGAEvent('HOME', 'CLICK', 'DETAILS', eventName, {eventDesc:eventDesc, 
					contactNo:contactNo, 
					emailId:emailId })
	$('#detail_event_name').html(eventName)
	$('#detail_desc').html(eventDesc)

	// if(contactNo != "None")
	// 	$('#detail_contact_no').html(contactNo)
	// else
	// 	$('#detail_contact_no').html("No Details")

	if(window['IS_MOBILE'] && contactNo != 'None')
		$('#detail_contact_no_wrapper').attr('href','tel:+91' + contactNo)

	// if(emailId!= "None")
	// 	$('#detail_email_id').html(emailId)
	// else
	// 	$('#detail_email_id').html("No Details")

	$('#detailsModal').openModal({
		dismissible: true,
		opacity: 0.9, // Opacity of modal background
      	in_duration: 300, // Transition in duration
      	out_duration: 400, // Transition out duration
      	starting_top: '4%', // Starting top style attribute
      	ending_top: '10%' // Ending top style attribute
	});
}

function handleDetailModalMobile(){
	contact = $('#detail_contact_no').text()
	trackGAEvent('HOME', 'CLICK', 'DETAIL_PHONE_NO',contact)
}

$(document).ready(function () {

	if(!IS_MOBILE){
		$('#header-logo').addClass('hide')
	}
	
	//initializing side flyout menu
	$("#flyout-menu-trigger").sideNav({
		'edge':'left'
	});
    $('.collapsible').collapsible();

    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	var dismissible = isCityPresent == '0' ? false : true
	$('#area-modal-trigger').leanModal({
		dismissible:dismissible,
		opacity: isCityPresent == '0' ? 0.9 : 0.7, // Opacity of modal background
      	in_duration: 300, // Transition in duration
      	out_duration: 400, // Transition out duration
      	starting_top: '4%', // Starting top style attribute
      	ending_top: '10%' // Ending top style attribute
	});
	$('#area-modal-mobile-trigger').leanModal({
		dismissible:dismissible,
		opacity: isCityPresent == '0' ? 0.9 : 0.7, // Opacity of modal background
      	in_duration: 300, // Transition in duration
      	out_duration: 400, // Transition out duration
      	starting_top: '4%', // Starting top style attribute
      	ending_top: '10%' // Ending top style attribute
	});	
  	
  	$('select').material_select();

	if(isCityPresent == '0'){
		trackGAEvent('HOME', 'DEFAULT', 'CITY_ABSENT','')
		$('#area-modal-trigger').click()
		$('#area-modal-mobile-trigger').click()
	}

	$('.scrollspy').scrollSpy({
		scrollOffset:200
	});

	$('document').on('click', '#lc_chat_layout', handleContactNowClick)
});         