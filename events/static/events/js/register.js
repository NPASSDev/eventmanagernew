$('#addEvent').addClass('active')
var startDatePicker
var endDatePicker
function submitForm(){
    $('#page-loader').show()  
    $('#spinner').removeClass('hide')  
    $('#submit_btn').prop('disabled', true)  
    $('#error_div').addClass('hide')
    var startDate = startDatePicker.pickadate('picker').get('select','yyyy-mm-dd')
    var endDate = endDatePicker.pickadate('picker').get('select','yyyy-mm-dd')
    var data = new FormData()
    data.append('csrfmiddlewaretoken',CSRF_TOKEN)
    if(document.getElementById('imageFile').files[0])
        data.append('Image',document.getElementById('imageFile').files[0])
    
    data.append('Name',$('#event_name').val())
    data.append('StartDate',startDate)
    data.append('EndDate',endDate)
    data.append('AreaId',parseInt($('#venueSelect').find(":selected")[0].value))
    data.append('CategoryId',parseInt($('#categorySelect').find(":selected")[0].value))
    data.append('Description',$('#desc').val())
    data.append('Address',$('#address').val())
    data.append('ContactPhone',$('#mobile').val())
    data.append('ContactEmail',$('#email_id').val())
    
    var validationMsg = validateForm(data)
  
    if(validationMsg != ""){
        trackGAEvent('ADD_EVENT', 'VALIDATION', 'VALIDATION_MSG',validationMsg)
        $('#error_div').first().removeClass('hide')
        $('#error_div').first().text(validationMsg)
        $('#spinner').addClass('hide')
        $('#submit_btn').prop('disabled', false)
        $('#page-loader').hide()
        return
    } 

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '', true);
    xhr.onload = function (data) {
        if (xhr.status === 200 && data.isValid) {
            trackGAEvent('ADD_EVENT', 'SUCCESS', 'EVENT_ADDED',data.msg)
            Materialize.toast(data.msg, 12000)
            window.location.href = '/'
        } 
        else {
            trackGAEvent('ADD_EVENT', 'VALIDATION', 'VALIDATION_MSG',data.msg)
            $('#page-loader').hide()
            $('#error_div').first().removeClass('hide')
            $('#spinner').addClass('hide')  
            $('#submit_btn').prop('disabled', false)
            $('#error_div').first().text(data.msg)
        }
    };
    xhr.send(data)
}

function validateForm(data){
    var msg = ""
    var alphaNumericRegex = new RegExp(/^[0-9a-zA-Z\s]*$/)
    var emailRegex = new RegExp(/^([A-Z|a-z|0-9](\.|_){0,1})*[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])*((\.){0,1}[A-Z|a-z|0-9]){1}\.[a-z]{2,}$/)
    var mobileRegex = new RegExp(/^[7-9][\d]{9}$/)
    ALLOWED_IMAGE_TYPES = ['image/png', 'image/jpeg', 'image/jpg']
    
    if(!data.get('Name')){
        msg = "We need the event name to proceed!"
    }
    else if(!alphaNumericRegex.test(data.get('Name'))){
        msg = "Only alphabets and numbers allowed in event name. Check again?"
    }
    else if(!alphaNumericRegex.test(data.get('Name'))){
        msg = "Only alphabets and numbers allowed in event name. Check again?"
    }
    else if(!data.get('StartDate')){
        msg = "Please enter a start date for the event"
    }
    else if(!data.get('EndDate')){
        msg = "Please enter a end date for the event"
    }
    else if(Date.parse(data.get('EndDate')) < Date.parse(data.get('StartDate'))){
        msg = "Start date should be prior to end date. Check again?"
    }
    else if(Date.parse(data.get('EndDate')) < Date.parse(new Date())){
        msg = "Oh! you cannot add already ended events. Check again?"
    }
    else if(!data.get('CategoryId') || isNaN(data.get('CategoryId'))){
        msg = "Please select the event category"
    }
    else if(!data.get('AreaId') || isNaN(data.get('AreaId'))){
        msg = "Please select the event venue"
    }
    else if(!data.get('Address')){
        msg = "Please enter event address"
    }
    else if(data.get('ContactEmail') && !emailRegex.test(data.get('ContactEmail'))){
        msg = "Oh! That doesn't look a valid email id"
    }
    else if(data.get('ContactPhone') && !mobileRegex.test(data.get('ContactPhone'))){
        msg = "Oh! That doesn't look a valid mobile number"
    }
    /*else if(!data.get('ContactPhone') && !data.get('ContactEmail')){
        msg = "Oh! We need either mobile or email id contact for this event"
    }*/
    else if(!data.get('Description') || data.get('Description').length < 50){
        msg = "Oh! We need a event description of atleast 50 characters"
    }
    else if(data.get('Image') != "undefined" && data.get('Image') != undefined
         && ALLOWED_IMAGE_TYPES.indexOf(data.get('Image')['type']) == -1){
        msg = "Please upload the image in either png, jpeg format"
    }

    return msg
}

$(document).ready(function() {
  startDatePicker = $('#start_date').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });
  endDatePicker = $('#end_date').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });
  $('select').material_select();
});          
