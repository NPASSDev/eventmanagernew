import requests
import xlwt
from tempfile import TemporaryFile
from BeautifulSoup import BeautifulSoup
import xlrd
from xlrd import open_workbook
import cloudinary
import cloudinary.uploader
import cloudinary.api
import datetime
import codecs
import re
import apiai
import json

def scrapeData():
	resp = requests.get('http://www.whatsupgoa.com/GoaFeature/calendar.php')
	soup = BeautifulSoup(resp.text)
	events = soup.findAll('div', {"class":"event"})
	book = xlwt.Workbook()
	sheet = book.add_sheet('sheet1')
	APIAI_TOKEN = '25da83c4b606487b826a593aaacf6a42'
	ai = apiai.ApiAI(APIAI_TOKEN)
	req = ai.text_request()
	output = []
	for i, event in enumerate(events):
		try:
			#print i
			outputObj = {}
			outputObj['Name'] = event.findAll("h4")[0].contents[0]
			outputObj['ImageLocation'] = event.findAll("a")[0]['href']
			outputObj['StartDate'] = event.findAll("p", {"class":"date"})[0].findAll("em")[0].contents[0]
			outputObj['EndDate'] = outputObj['StartDate']
			outputObj['Address'] = event.findAll("p", {"class":"texte"})[0].findAll("strong")[0].contents[2]
			parent = event.findAll("p", {"class":"texte"})[0]
			desc = ''
			for line in parent.contents:
				if hasattr(line,'text'):
					desc = desc + ' ' + line.text
				else:
					desc = desc + ' ' + line
			outputObj['Description'] = desc
			req.query = outputObj['Name']
			resp = req.getresponse()
			jsonResponse = json.loads(resp.read())
			outputObj['Category'] = jsonResponse['result']['metadata']['intentName']
			for j, key in enumerate(outputObj):
				sheet.write(i, j, outputObj[key])
		except:
			pass

	name = "random1.xls"
	book.save(name)
	book.save(TemporaryFile())

def scrapeMumbaiData():
	#resp = requests.get('http://www.whatshot.in/mumbai/events')
	resp = codecs.open("/home/sharmad/Desktop/test.html").read()
  	print resp
	soup = BeautifulSoup(resp)
	events = soup.findAll('div',{'class':'work-item feeds-data'})
	book = xlwt.Workbook()
	sheet = book.add_sheet('sheet1')

	output = []
	for i, event in enumerate(events):
		#try:
			#print i
		outputObj = {'Category':'','StartDate':'','EndDate':'','Name':'','ImageLocation':'','Address':'','Description':'','Mobile':''}
		detailUrl = event.findAll('a')[0]['href']
		detailResp = requests.get(detailUrl)
		detailSoup = BeautifulSoup(detailResp.text)
		
		try:
			outputObj['Category'] = event.findAll('div',{'class':'maintag'})[0].text
		except:
			pass

		try:	
			date = detailSoup.findAll('div',{'class':'venue'})[0].contents[3].replace('\n','').replace('\t','')
			dateArray = date.split(' ')
			now = datetime.datetime.now()
			startDateString = dateArray[0] + ' ' + dateArray[1] + ' ' + str(now.year)
			startDateString = startDateString.replace('st','').replace('nd','').replace('rd','').replace('th','')
			startDateObj = datetime.datetime.strptime(startDateString,'%d %b %Y')
			outputObj['StartDate'] = startDateObj.strftime('%d/%m/%Y')

			if len(dateArray) > 5:
				endDateString = dateArray[2] + ' ' + dateArray[3] + ' ' + str(now.year)
				endDateString = dateString.replace('st','').replace('nd','').replace('rd','').replace('th','')
				endDateObj = datetime.datetime.strptime(endDateString,'%d %b %Y')
				outputObj['EndDate'] = endDateObj.strftime('%d/%m/%Y')
				time = dateArray[4] + dateArray[5] + dateArray[6]
			else:
				outputObj['EndDate'] = startDateObj.strftime('%d/%m/%Y')
				time = dateArray[2] + dateArray[3] + dateArray[4]
		except:
			pass

		outputObj['Name'] = event.findAll('div',{'class':'feed-title'})[0].text
		outputObj['ImageLocation'] = event.findAll('img')[1]['data-src']
		
		try:
			outputObj['Address'] = detailSoup.findAll('a',{'data-ga-label':'venue'})[0].text
		except:
			pass

		try:
			outputObj['Description']  = time + '\n' + re.sub('<[^<]+?>', '', str(detailSoup.findAll('div',{'class':'detail'})[0].findAll('p')[0]))
		except:
			pass
		#outputObj['Description']  = time + '\n' + detailSoup.findAll('div',{'class':'detail'})[0].findAll('p')[0]
			
		try:
			contactUrl = detailSoup.findAll('a',{'data-ga-label':'venue'})[0]['href']
			contactResp = requests.get(contactUrl)
			contactSoup = BeautifulSoup(contactResp.text)
			outputObj['Mobile'] = contactSoup.findAll('div',{'class':'phone'})[0].findAll('div')[1].text
		except:
			pass

		print i
		for j, key in enumerate(outputObj):
			print i
			print type(outputObj[key])
			print outputObj[key]
			sheet.write(i,j,outputObj[key])
		#except:
			#raise
			#pass

	name = "MumbaiEvents.xls"
	book.save(name)
	book.save(TemporaryFile())


def saveToDB():
	book = open_workbook('random1.xls')
	sheet = book.sheet_by_index(1)
	cols = ['EventCategoryId','StartDate', 'EndDate', 'Name', 'ImageLocation', 'Address', 'Description']
	OwnerId = User.objects.get(EmailId='sharmad91@gmail.com')
	AreaId = Cities.objects.get(Name='Goa')
	for row_index in range(0, sheet.nrows):
		try:
			currEvent = Event()
			for col_index in range(0, 7):
				if col_index is 4:
					upload = cloudinary.uploader.upload(sheet.cell(row_index, col_index).value,
	                        folder='events/')
					setattr(currEvent, cols[col_index], upload['url'])
				elif col_index is 0:
					category = Category.objects.get(Category=sheet.cell(row_index, col_index).value)
					setattr(currEvent, cols[col_index], category)
				else:
					setattr(currEvent, cols[col_index], sheet.cell(row_index, col_index).value)
				# print row_index, col_index
				# print sheet.cell(row_index, col_index)
			currEvent.OwnerId = OwnerId
			currEvent.AreaId = AreaId
			currEvent.save()
		except:
			pass
