from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import User, Category, UserCategoryMap, Event, Cities
from .forms import UserForm, EventForm, UserRegistrationForm, UserAuthenticationForm
import unicodedata
import json
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from .models import Document
from .forms import DocumentForm
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.template import RequestContext
from django.contrib.auth import login as django_login, authenticate, logout as django_logout
import cloudinary
import cloudinary.uploader
import cloudinary.api
from datetime import datetime
from base import utils, mailtemplates, smstemplates
#def login(request):
#    return render(request, 'events/login.html')


def cities(request, city='Goa'):
    #import ipdb; ipdb.set_trace();
    citylist=Cities.objects.all()
    categories = Category.objects.all()
    username = ''
    cityObj = Cities.objects.filter(Name=city)
    if request.user.is_authenticated() and hasattr(request.user, 'Name'):
        username = request.user.Name
    else:
        username = ""

    if(cityObj is not None):
        eventObj = Event.objects.filter(AreaId=cityObj,EndDate__gte=datetime.now()).order_by('-Type','StartDate')
        eventlist= eventObj.values('id','Name', 'StartDate', 'ImageLocation',
            'EndDate', 'Description', 'ContactPhone', 'ContactEmail', 'AreaId__Name',
            'EventCategoryId__Category', 'Address', 'Type')
        response = render(request, 'events/home1.html',
        {
            #'query_results':json.loads(serializers.serialize('json', eventlist)), 
            'eventList':eventlist, 
            'eventListJSON':serializers.serialize('json', eventObj),  
            'categories':categories.values(), 
            'categoriesJSON':serializers.serialize('json', categories),
            'cities':citylist.values(),
            'isCityPresent':'1',
            'cityName':city,
            'username': username,
            'isLoggedIn':request.user.is_authenticated(),
            'isMobile':request.user_agent.is_mobile
        })
        response.set_cookie('area',city)
    else:
		response = HttpResponseRedirect(reverse('home'))
		response.delete_cookie('area')
        
    return response

def home(request):
    #import ipdb; ipdb.set_trace();
    citylist=Cities.objects.all()
    username = ''
    area = request.COOKIES.get('area')
    
    if request.user.is_authenticated() and hasattr(request.user, 'Name'):
        username = request.user.Name
    else:
        username = ""

    if(area and Cities.objects.filter(Name=area).count() > 0):
    	return HttpResponseRedirect(reverse('cities', args=[area]))

    return render(request, 'events/home1.html',
        {
            'eventList':[], 
            'eventListJSON':serializers.serialize('json', []), 
            'categories':[], 
            'categoriesJSON':serializers.serialize('json', []), 
            'cities':citylist.values(),
            'isCityPresent':'0',
            'cityName':'',
            'username': username,
            'isLoggedIn':request.user.is_authenticated(),
            'isMobile':request.user_agent.is_mobile
        })

def user_new(request):
        if request.method == "POST":
                form = UserForm(request.POST)
                if form.is_valid():
                        user = form.save(commit=False)
                        user.save()
			selectedevent = request.POST.getlist('selectedevent[]')
			print("this is ",selectedevent)
			for i in selectedevent:
				userevent=UserCategoryMap()
				userevent.UserId= User.objects.get(id=user.pk)
				userevent.EventCategoryId=Category.objects.get(id=i)
				userevent.save()
			eventtype=UserCategoryMap.objects.values_list('EventCategoryId').filter(UserId=user.pk)
			print(eventtype)
			events=Event.objects.filter(EventCategoryId__in=eventtype)
			for i in events:
				print(i.Name)
			return render(request, 'events/viewevents.html' , {'query_results':events})                        
        else:
			form = UserForm()
			options = Category.objects.all()
			return render(request,'events/user.html', {'form': form ,'options': options})

def event_new(request):
    # if request.user.is_authenticated():
    if request.method == 'POST':
        form = EventForm(request.POST, request.FILES)
        if form.is_valid():
            event = Event()
            event = form.save(commit=False)
            event.EventCategoryId = Category.objects.get(pk=request.POST['CategoryId']) 
            event.AreaId = Cities.objects.get(pk=request.POST['AreaId'])
            event.OwnerId = User.objects.get(pk=request.user.id)
            if request.FILES and request.FILES['Image']:
                upload = cloudinary.uploader.upload(request.FILES['Image'],
                        folder='events/')
                event.ImageLocation = upload['url']
            event.save()

            #Email Sending Part
            template = mailtemplates.getEventRegisterTemplate()
            recipientEmailList = [request.user.EmailId, request.POST['ContactEmail']] if request.POST['ContactEmail'] else [request.user.EmailId] 
            template["body"] = template["body"].replace('%%event_name%%',request.POST['Name'])
            template["body"] = template["body"].replace('%%user%%',request.user.Name)
            template["subject"] = template["subject"].replace('%%event_name%%',request.POST['Name'])
            utils.sendEmail(template["body"], template["subject"], recipientEmailList)
            
            #SMS Sending part
            recipientMobileList = [request.user.MobileNumber, request.POST['ContactPhone']] if request.POST['ContactPhone'] else [request.user.MobileNumber]
            smsTemplate = smstemplates.getEventRegisterTemplate()
            smsTemplate["body"] = smsTemplate["body"].replace('%%event_name%%',request.POST['Name'])
            utils.sendSMS(smsTemplate["body"], smsTemplate["campaign"], recipientMobileList)

            responseObj = {'isValid':True, 'msg':'success'}
            return HttpResponse(json.dumps(responseObj))
        else:
            responseObj = {'isValid':False, 'msg':'Please check the form'}
            return HttpResponse(json.dumps(responseObj))
    else:
        form = EventForm()
        citylist = Cities.objects.all()
        categories = Category.objects.all()
        return render(request, 'events/events.html', {'categories': categories,
                      'cities':citylist})

def list(request):
    
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()

            
            return HttpResponseRedirect(reverse('events.views.list'))
    else:
        form = DocumentForm() 

    
    documents = Document.objects.all()

    
    return render(request, 'events/list.html', {'documents': documents, 'form': form})
        
def login(request):
    """
    Log in view
    """
    if request.method == 'POST':
        form = UserAuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(EmailId=request.POST['EmailId'], password=request.POST['Password'])
            if user is not None:
                if user.is_active:
                    django_login(request, user)
                    responseData = {"isValid":True, "msg":"Successfully logged in. Please wait while we redirect you..."}
                    return HttpResponse(json.dumps(responseData))
        
        responseData = {"isValid":False, "msg":"Invalid username or password"}
        return HttpResponse(json.dumps(responseData))
    #     return  render_to_response('events/userlogin.html', {
    #      'form': form,'error_msg':"Email or password incorrect",
    # }, context_instance=RequestContext(request))
    else:
        form = UserAuthenticationForm()
    return render_to_response('events/userlogin.html', {
        'form': form,
    }, context_instance=RequestContext(request))   

def register(request):
    """
    User registration view.
    """
    options = Category.objects.all()
    if request.method == 'POST':
        print(request.POST)
        form = UserRegistrationForm(data=request.POST)
        print(form.is_valid())
        print(request.POST['EmailId'])
        if form.is_valid():
            user = form.save()

            #Email sending part
            emailTemplate = mailtemplates.getUserRegisterTemplate()
            emailTemplate["body"] = emailTemplate["body"].replace('%%user%%',request.POST['Name'])
            utils.sendEmail(emailTemplate["body"], emailTemplate["subject"], [request.POST['EmailId']])
            
            #sms sending part
            smsTemplate = smstemplates.getSignUpMsg()
            smsTemplate["body"] = smsTemplate["body"].replace('%%user%%',request.POST['Name'])
            utils.sendSMS(smsTemplate["body"], smsTemplate["campaign"],[request.POST['MobileNumber']])

            selectedevent = request.POST.getlist('selectedevent[]')
            print("this is ",selectedevent)
            for i in selectedevent:
                userevent=UserCategoryMap()
                userevent.UserId=User.objects.get(id=user.pk)
                print(user.pk)
                userevent.EventCategoryId=Category.objects.get(id=i)
                userevent.save()
                print(int(i))
            user1 = authenticate(EmailId=request.POST['EmailId'], password=request.POST['password1'])
            print(user.EmailId,user.password)
            if user1 is not None:
                if user1.is_active:
                    django_login(request, user1)
                    print("user is active")
                    responseData = {"isValid":True, "msg":"Successfully Signed Up! Please wait while we redirect you..."}
                    return HttpResponse(json.dumps(responseData))
                    #return redirect('/events')
        else:
            responseData = {"isValid":False, "msg":"Email Id or Mobile Number already exists."}
            return HttpResponse(json.dumps(responseData))
        
    else:
	
        form = UserRegistrationForm()
    return render_to_response('events/userregister.html', {
        'form': form, 'options': options 
    }, context_instance=RequestContext(request))

def logout(request):
    """
    Log out view
    """
    django_logout(request)
    return redirect('/')