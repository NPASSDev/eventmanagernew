from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^register/$',views.register,name='register'),
    url(r'^login/$', views.login, name='login'),
    url(r'^list/$', views.list, name='list'),
    # url(r'^$', views.home, name='home'),
    url(r'^$', views.cities, name='HomeCities'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^user/$',views.user_new,name='user_new'),
#     url(r'^showevents',views.home),
    url(r'^details/$', views.event_new , name='event_new'),
    url(r'^(?P<city>[\w\-]+)/$', views.cities, name='cities'),   
    
   
]
