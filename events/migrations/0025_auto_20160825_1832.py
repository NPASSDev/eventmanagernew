# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-25 18:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0024_auto_20160821_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='ImageLocation',
            field=models.URLField(null=True),
        ),
    ]
