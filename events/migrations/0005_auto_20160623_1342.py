# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-06-23 13:42
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_event_eventcategory'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usereventmap',
            name='EventId',
        ),
        migrations.AddField(
            model_name='usereventmap',
            name='EventCategory',
            field=models.CharField(default=datetime.datetime(2016, 6, 23, 13, 42, 30, 988916, tzinfo=utc), max_length=200),
            preserve_default=False,
        ),
    ]
