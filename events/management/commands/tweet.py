from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Tweets a random event with eventry account. Used for auto tweeting through cron.'

    def handle(self, *args, **options):
        from events.models import Event
        import tweepy, random, requests, unicodedata, os
        from datetime import datetime, timedelta

        CONSUMER_KEY = 'FnKud4Ulc8r1PSVdxb7R9UYCD'#keep the quotes, replace this with your consumer key
        CONSUMER_SECRET = 'ep8MXiFoFwYm8Kps86TBQBbMprdoMv3vndFyYF9b91SKtLVCCp'#keep the quotes, replace this with your consumer secret key
        ACCESS_KEY = '769492362649141248-qvbuU3JCCpJi9VbIHqRUrpRUoz5KeAe'#keep the quotes, replace this with your access token
        ACCESS_SECRET = 'Gj9IaN2r2tiGU1p2SgE7GlugFRwS97ZF5t6fw3pcPM3Sg'#keep the quotes, replace this with your access token secret
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
        api = tweepy.API(auth)

        event = random.choice(Event.objects.filter(EndDate__gte=datetime.now(), EndDate__lte=datetime.now() + timedelta(days=7)).values('Name', 'AreaId__Name', 'StartDate', 'id', 'ImageLocation'))
        msg = "Enjoy " + event['Name'] + " in " + event['AreaId__Name'] + " on " + event['StartDate'].strftime("%d %b, %y") + " "
        msg = msg + "http://eventry.in/" + event['AreaId__Name'] + "/#e" + str(event['id'])
        url = unicodedata.normalize('NFKD', event["ImageLocation"]).encode('ascii','ignore')
        filename = 'temp.jpg'
        request = requests.get(url, stream=True)
        if request.status_code == 200:
            with open(filename, 'wb') as image:
                for chunk in request:
                    image.write(chunk)
            api.update_with_media(filename, status=msg)
            os.remove(filename)
            print msg