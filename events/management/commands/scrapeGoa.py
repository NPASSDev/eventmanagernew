from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Scrapes events from Goa and uploads them to database.'

    def handle(self, *args, **options):
		from events.models import Event, Category, User, Cities
		import requests
		import xlwt
		from tempfile import TemporaryFile
		from BeautifulSoup import BeautifulSoup
		import xlrd
		from xlrd import open_workbook
		import cloudinary
		import cloudinary.uploader
		import cloudinary.api
		import datetime
		import codecs
		import re
		import os
		from datetime import datetime, timedelta
		from base import utils
		import apiai
		import json
		#code to scrape data
		filename = 'goa-events.xls'
		#removing existing data
		try:
			os.remove(filename)
		except:
			pass
		cloudinary.api.delete_resources_by_prefix('events/');
		Event.objects.all().delete()

		resp = requests.get('http://www.whatsupgoa.com/GoaFeature/calendar.php')
		soup = BeautifulSoup(resp.text)
		events = soup.findAll('div', {"class":"event"})
		book = xlwt.Workbook()
		sheet = book.add_sheet('sheet1')
		APIAI_TOKEN = '25da83c4b606487b826a593aaacf6a42'
		ai = apiai.ApiAI(APIAI_TOKEN)
		req = ai.text_request()
		output = []
		for i, event in enumerate(events):
			try:
				#print i
				outputObj = {}
				outputObj['Name'] = event.findAll("h4")[0].contents[0].title()
				outputObj['ImageLocation'] = event.findAll("a")[0]['href']
				outputObj['StartDate'] = event.findAll("p", {"class":"date"})[0].findAll("em")[0].contents[0]
				outputObj['EndDate'] = outputObj['StartDate']
				outputObj['Address'] = event.findAll("p", {"class":"texte"})[0].findAll("strong")[0].contents[2]
				parent = event.findAll("p", {"class":"texte"})[0]
				desc = ''
				if datetime.strptime(outputObj['StartDate'],'%Y-%m-%d') > datetime.now() + timedelta(days=7):
					print('broke at  ' + outputObj['StartDate'])
					break
				for line in parent.contents:
					if hasattr(line,'text'):
						desc = desc + line.text
					else:
						desc = desc + line
				outputObj['Description'] = desc
				req = ai.text_request()
				req.query = outputObj['Name']
				resp = req.getresponse()
				jsonResponse = json.loads(resp.read())
				outputObj['Category'] = jsonResponse['result']['metadata']['intentName']
				for j, key in enumerate(outputObj):
					sheet.write(i, j, outputObj[key])
			except:
				pass

		book.save(filename)
		book.save(TemporaryFile())

	#	code to write scrapped data to DB
		book = open_workbook(filename)
		sheet = book.sheet_by_index(0)
		cols = ['EventCategoryId', 'StartDate', 'EndDate', 'Name', 'ImageLocation', 'Address', 'Description']
		OwnerId = User.objects.get(EmailId='sharmad91@gmail.com')
		AreaId = Cities.objects.get(Name='Goa')
		for row_index in range(0, sheet.nrows):
			try:
				currEvent = Event()
				for col_index in range(0, 7):
					if col_index is 0:
						category = Category.objects.get(Category=sheet.cell(row_index, col_index).value)
					elif col_index is 4:
						upload = cloudinary.uploader.upload(
							sheet.cell(row_index, col_index).value.replace('../dyn/','http://whatsupgoa.com/dyn/'),
		                        folder='events/')
						setattr(currEvent, cols[col_index], upload['url'])
					else:
						setattr(currEvent, cols[col_index], sheet.cell(row_index, col_index).value)
					# print row_index, col_index
					# print sheet.cell(row_index, col_index)
				setattr(currEvent, 'EventCategoryId', category)
				currEvent.OwnerId = OwnerId
				currEvent.AreaId = AreaId
				currEvent.save()
			except:
				pass

		utils.sendEmail("Successfully uploaded data", 
			"Eventry Data Upload Successfull on " + datetime.now().strftime("%B %d, %Y"), ['sharmad91+eventry@gmail.com'])
		utils.sendSMS("Eventry Data Upload Successfull on " + datetime.now().strftime("%B %d, %Y"), "dataUpload",["9049253865"])
