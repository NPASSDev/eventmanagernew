from django.contrib import admin
from events.models import Event, Category, Cities

admin.site.register(Event)
admin.site.register(Category)
admin.site.register(Cities)
# Register your models here.
