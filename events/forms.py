
from django import forms
from django.forms import ModelForm
from .models import User, Event
from django.forms.fields import DateField, ChoiceField
from django.forms import widgets

from django.forms.widgets import RadioFieldRenderer
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

class DateInput(forms.DateInput):
    input_type = 'date'

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('Name','EmailId','MobileNumber')

	widgets = {
		'Name': forms.TextInput(attrs={'class':'form-control'}),
		'EmailId': forms.TextInput(attrs={'class':'form-control'}),
	}
class EventForm(forms.ModelForm): 
    class Meta: 
	model = Event 
	fields = ('Name', 'StartDate', 'EndDate', 'Description', 'ContactPhone', 'Address', 'ContactEmail')

class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        
    )

class RadioFieldWithoutULRenderer(RadioFieldRenderer):

    def render(self):
	return  mark_safe(u'%s' % u'\n'.join([u'%s'  % force_unicode(w) for w in self]))




class UserRegistrationForm(forms.ModelForm):
    EmailId = forms.EmailField(widget=forms.TextInput,label="Email")
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label="Confirm Password")
    Notifications = forms.BooleanField()
    #CHOICES = (('Male', 'Male',), ('Female', 'Female',))
    #Gender =  forms.ChoiceField(widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer), choices=CHOICES, label="Gender")
    class Meta:
        model = User
        fields = ('Name','EmailId','MobileNumber','password1', 'password2', 'Notifications')
#        widgets = {

#             'Gender': forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES),
#             'Gender': forms.TypedChoiceField(choices=CHOICES, widget=forms.RadioSelect()),

#        }
#        widgets = {
#                'Name': forms.TextInput(attrs={'class':'form-control'}),
#                'EmailId': forms.EmailInput(attrs={'class':'form-control'}),
#		'MobileNumber':forms.NumberInput(attrs={'class':'form-control'}),
#        }
        

    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        cleaned_data = super(UserRegistrationForm, self).clean()
        # if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
        #     if self.cleaned_data['password1'] != self.cleaned_data['password2']:
        #         raise forms.ValidationError("Passwords don't match. Please enter both fields again.")
        return self.cleaned_data

    def save(self, commit=True):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    def validate():
        return True

class UserAuthenticationForm(forms.Form):
    """
    Login form
    """
    EmailId = forms.EmailField(widget=forms.TextInput)
    Password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        fields = ['EmailId', 'Password']


