from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractBaseUser

#class User(models.Model):
#	Name = models.CharField(max_length=200)
#	EmailId = models.EmailField()
#	MobileNumber = models.CharField(max_length=200)
#	Age = models.IntegerField()
#	Gender = models.CharField(max_length=10)
#	BriefDescription = models.TextField()

class Category(models.Model):
	Category = models.CharField(max_length=200, db_index=True)
# Create your models here.


class UserCategoryMap(models.Model):
	UserId = models.ForeignKey('User', on_delete=models.CASCADE)
	EventCategoryId = models.ForeignKey('Category', on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)
       
class Event(models.Model):
	Name = models.CharField(max_length=200)
	StartDate = models.DateField()
	EndDate = models.DateField()
	#StartTime = models.TimeField()
	#EndTime = models.TimeField()
	AreaId = models.ForeignKey('Cities', default=None, on_delete=models.CASCADE, db_index=True)
	EventCategoryId = models.ForeignKey('Category', on_delete=models.CASCADE)
	ImageLocation = models.URLField(null=True)
	Description = models.TextField()
	Address = models.TextField()
	ContactPhone = models.TextField(max_length=15, null=True)
	ContactEmail = models.TextField(max_length=200, null=True)
	Type = models.TextField(max_length=50, null=True)
	OwnerId = models.ForeignKey('User', on_delete=models.CASCADE, default=None)
	IsApproved = models.BooleanField(default=True)
	IsActive = models.BooleanField(default=True)
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)


class Cities(models.Model):
	Name=models.CharField(max_length=200, db_index=True)

class Document(models.Model):
    docfile = models.FileField(upload_to='documents')

class User(AbstractBaseUser):
	Name = models.CharField(max_length=200)
	EmailId = models.EmailField('email address', unique=True, db_index=True)
	MobileNumber = models.CharField(max_length=200, unique=True, db_index=True)
	Notifications = models.BooleanField(default=True)
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)
	admin = models.BooleanField(default=False) 

	USERNAME_FIELD = 'EmailId'

	def __unicode__(self):
		return self.EmailId

	def get_full_name(self):
		return self.Name

	def get_short_name(self):
		return self.Name

	@property
	def is_superuser(self):
		return self.admin

	@property
	def is_staff(self):
		return self.admin

	def has_perm(self, perm, obj=None):
		return self.admin

	def has_module_perms(self, app_label):
		return self.admin