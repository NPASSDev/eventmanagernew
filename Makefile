deps:
	pip install -r requirements.txt

run:
	python manage.py runserver 0.0.0.0:8000

runp:
	python manage.py runserver_plus

rung:
	honcho -f Procfile start web

env:
	source venv/bin/activate
	
shell:
	python manage.py shell_plus

url:
	python manage.py show_urls

validate:
	python manage.py validate_templates

gunicorn-prod:
	authbind gunicorn base.wsgi:application -b 0.0.0.0:80 &

