import smtplib
import settings
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import requests

def sendEmail(body, subject, recipient):
	# Define to/from
	sender = settings.EMAIL_HOST_USER
	# Create message
	msg = MIMEMultipart('alternative')
	msgBody = MIMEText(body, 'html')
	msg.attach(msgBody)
	msg['Subject'] = subject
	msg['From'] = sender
	msg['To'] = ",".join(recipient )
	msg.add_header('reply-to', settings.REPLY_TO_ADDRESS)
	# Create server object with SSL option
	server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)

	# Perform operations via server
	server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
	server.sendmail(sender, recipient, msg.as_string())
	server.quit()

def sendSMS(body, campaign, recipient):
	api = settings.SMS_API
	api = api + '&mobiles=' + ','.join(recipient)
	api = api + '&message=' + body
	api = api + '&campaign=' + campaign
	api = api + '&response=JSON'

	for key, value in settings.SMS_CONSTANTS.iteritems():
		api = api + '&' + key + '=' + value
	requests.get(api)