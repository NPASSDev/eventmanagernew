def getSignUpMsg():
	template = {'body':'', 'campaign':''}
	template['body'] = "Hi %%user%%!\nYour account on Eventry has been created. Feel free to contact us @ support@eventry.in for any questions \n - Team Eventry"
	template['campaign'] = "SIGNUP"
	return template

def getEventRegisterTemplate():
	template = {'body':'', 'campaign':''}
	body = "Hi, "
	body = body + "Your event %%event_name%% has been registered. "
	body = body + "In case of any issues please contact us @ support@eventry.in.\n-Team Eventry"

	template['campaign'] = "NEW_EVENT"
	template['body'] = body
	return template
