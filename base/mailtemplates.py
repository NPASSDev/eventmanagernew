def getUserRegisterTemplate():
	template = {'body':'', 'subject':''}
	template['body'] = "Welcome to Eventry %%user%%!  <br /> Feel free to contact us @ <b> support@eventry.in </b> for any questions <br/> <br />-Team Eventry"
	template['subject'] = "Welcome to Eventry!"
	return template

def getEventRegisterTemplate():
	template = {'body':'', 'subject':''}
	body = "Hello %%user%%, <br />"
	body = body + "Your event %%event_name%% has been registered. "
	body = body + "In case of any issues please contact us @ support@eventry.in. <br/> <br />-Team Eventry"

	template['subject'] = "Your event on Eventry - %%event_name%%"
	template['body'] = body
	return template